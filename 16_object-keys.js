let car = {  
    make: 'Honda',  
    model: 'Accord',  
    year: 2020  
  };  
//isi car
console.log(car);

//copy object car
let car_2 = car;
console.log(car_3);

//cara copy object entries
let car_3 = Object.entries(car);
console.log(car_2);

let keys = Object.keys(car);
//isi keys
console.log(keys);

//untuk menampilkan nilai value ada beberapa cara 
// cara 1
const nilai = Object.values(car);
console.log(nilai);

//cara 2
const nilai_2 = keys.map((key) => car[key]);
console.log(nilai_2);

//cara 3
const nilai_3 = keys.map(function(key2) {
 return car[key2];
});
console.log(nilai_3);