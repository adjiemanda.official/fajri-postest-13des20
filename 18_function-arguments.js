//cara 1
function math (satu,dua,tiga) {  
    proses=(dua*tiga)+satu;
    console.log(proses);  
  };

  math(53,61,67);

//cara 2
  function math_2 (satu,dua,tiga) {  
    proses=(dua*tiga)+satu;
    return proses;  
  };

  console.log(math_2(53,61,67));

//cara 3
const math_3 = (a,b,c) => (b*c)+a;
console.log(math_3(53,61,67));