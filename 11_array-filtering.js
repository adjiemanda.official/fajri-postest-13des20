let numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

//cara 1 nama fungsi evenNumbers bisa dihilangkan
let filtered = numbers.filter(function evenNumbers (number) {  
    return number % 2 === 0  
  })

//cara 2
function kelipatanTiga (number) {  
    return number % 3 === 0  
  }
let filter_2 = numbers.filter(kelipatanTiga);

//cara 3
let filter_3 = numbers.filter((number)=> number % 4 === 0);

console.log(filtered);
console.log(filter_2);
console.log(filter_3);

